#https://www.ardusub.com/developers/pymavlink.html
# Import mavutil
from flask import Flask,jsonify
from pymavlink import mavutil
import time
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from copy import copy
import threading

class drone:
    def __init__(self, url,db):
        self.db=db
        print("Iniciando")
        self.master = mavutil.mavlink_connection(url)
        self.heartbeat()
        self.telemetria={}
        t = threading.Thread(target=self.setUpdate,args=())
        t.start()
        
    def armar(self):
        self.master.mav.command_long_send(
            self.master.target_system,
            self.master.target_component,
            mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
            0,1, 0, 0, 0, 0, 0, 0)
    def desarmar(self):
        self.master.mav.command_long_send(
            self.master.target_system,
            self.master.target_component,
            mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
            0,0, 0, 0, 0, 0, 0, 0)
    def takeoff(self,altitude):
        self.master.mav.command_long_send(self.master.target_system, self.master.target_component, 
                mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,
                0, # confirmation
                0, # param1
                0, # param2
                0, # param3
                0, # param4
                0, # param5
                0, # param6
                altitude)
        
    def modo_estabilizado(self):
        return self.setModoDeVoo("STABILIZE")
    def modo_guiado(self):
        return self.setModoDeVoo('GUIDED')
    def modo_loiter(self):
        return self.setModoDeVoo('LOITER')
    def modo_rtl(self):
        return self.setModoDeVoo('RTL')
    def setModoDeVoo(self,mode):
        if mode not in self.master.mode_mapping():
            print('Unknown mode : {}'.format(mode))
            print('Try:', list(self.master.mode_mapping().keys()))
            exit(1)
        mode_id = self.master.mode_mapping()[mode]
        # Set new mode
        # master.mav.command_long_send(
        #    master.target_system, master.target_component,
        #    mavutil.mavlink.MAV_CMD_DO_SET_MODE, 0,
        #    0, mode_id, 0, 0, 0, 0, 0) or:
        # master.set_mode(mode_id) or:
        self.master.mav.set_mode_send(
            self.master.target_system,
            mavutil.mavlink.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED,
            mode_id)
        # Check ACK
        ack = False
        while not ack:
            # Wait for ACK command
            ack_msg = self.master.recv_match(type='COMMAND_ACK', blocking=True)
            ack_msg = ack_msg.to_dict()
            # Check if command in the same in `set_mode`
            if ack_msg['command'] != mavutil.mavlink.MAVLINK_MSG_ID_SET_MODE:
                continue
            # Print the ACK result !
            resultado=mavutil.mavlink.enums['MAV_RESULT'][ack_msg['result']].description
            print(resultado)
            if(resultado=="Command ACCEPTED and EXECUTED"):
                return True
            else:
                return False
    def set_rc_channel_pwm(id, pwm=1500):
        """ Set RC channel pwm value
        Args:
            id (TYPE): Channel ID
            pwm (int, optional): Channel pwm value 1100-1900
        """
        if id < 1:
            print("Channel does not exist.")
            return

        # We only have 8 channels
        #http://mavlink.org/messages/common#RC_CHANNELS_OVERRIDE
        if id < 9:
            rc_channel_values = [65535 for _ in range(8)]
            rc_channel_values[id - 1] = pwm
            self.master.mav.rc_channels_override_send(
            self.master.target_system,                # target_system
            self.master.target_component,             # target_component
                *rc_channel_values)                  # RC channel list, in microseconds.
    def circulo(self,altitude=0,qnt_voltas=1,raio=1,latitude=0,longitude=0):
        self.master.mav.command_long_send(self.master.target_system, self.master.target_component, 
                mavutil.mavlink.MAV_CMD_NAV_LOITER_TURNS,
                0,
                 qnt_voltas, # confirmation
                0, # param1
                raio, # param2
                 0, # param3
                 latitude, # param4
                 longitude, # param5
                 altitude)
    def heartbeat(self):
        self.master.wait_heartbeat()
    def getData(self):
        # params = self.master.recv_match(type='VFR_HUD', blocking=True)
        
        HEARTBEAT = self.master.recv_match(type='HEARTBEAT', blocking=True)
        SYS_STATUS = self.master.recv_match(type='SYS_STATUS', blocking=True)
        GPS_RAW_INT = self.master.recv_match(type='GPS_RAW_INT', blocking=True)
        GLOBAL_POSITION_INT = self.master.recv_match(type='GLOBAL_POSITION_INT', blocking=True)
        SCALED_PRESSURE = self.master.recv_match(type='SCALED_PRESSURE', blocking=True)
        ATTITUDE = self.master.recv_match(type='ATTITUDE', blocking=True)
        params = self.master.recv_match(type='VFR_HUD', blocking=True)
        print(params)
        timestamp=time.time()
        aux=Telemetria(
        airspeed=params.airspeed,
		latitude= GPS_RAW_INT.lat,
		longitude= GPS_RAW_INT.lon,
       
        satellites_visible=GPS_RAW_INT.satellites_visible,
        hdop=GPS_RAW_INT.eph,
		trueCourse= 0,
		heading= params.heading,
		headingBug= params.heading,
		groundSpeed= params.groundspeed, 
		airspeedBug= params.airspeed,	
		altitude= params.alt,
		altitudeBug= params.alt,		
		verticalSpeed= GLOBAL_POSITION_INT.vz,
		verticalSpeedBug= GLOBAL_POSITION_INT.vz,		
		bankAngle=ATTITUDE.roll,
		pitchAngle= ATTITUDE.pitch,
		turnCoordinationAngle= ATTITUDE.yaw,
		terrainAhead= 0,
        timestamp=timestamp,
        roll=ATTITUDE.roll,
        pitch=ATTITUDE.pitch,
        yaw=ATTITUDE.yaw,
        rollspeed=ATTITUDE.rollspeed,
        pitchspeed=ATTITUDE.pitchspeed,
        yawspeed=ATTITUDE.yawspeed,
        press_abs=SCALED_PRESSURE.press_abs,
        press_diff=SCALED_PRESSURE.press_diff,
        temperature=SCALED_PRESSURE.temperature,
        load=SYS_STATUS.load,
        voltage_battery=SYS_STATUS.voltage_battery,
        current_battery=SYS_STATUS.current_battery,
        relative_alt=GLOBAL_POSITION_INT.relative_alt,
        base_mode=HEARTBEAT.base_mode,
        custom_mode=HEARTBEAT.custom_mode


        )
        self.telemetria={
             "airspeed":params.airspeed,
            "latitude":GPS_RAW_INT.lat,
            "longitude": GPS_RAW_INT.lon,
            "satellites_visible":GPS_RAW_INT.satellites_visible,
            "hdop":GPS_RAW_INT.eph,
            "trueCourse": 0,
            "heading": params.heading,
            "headingBug": params.heading,
            "groundSpeed": params.groundspeed, 
            "airspeedBug": params.airspeed,	
            "altitude": params.alt,
            "altitudeBug": params.alt,		
            "verticalSpeed": GLOBAL_POSITION_INT.vz,
            "verticalSpeedBug": GLOBAL_POSITION_INT.vz,		
            "bankAngle":ATTITUDE.roll*-100,
            "pitchAngle": ATTITUDE.pitch*100,
            "turnCoordinationAngle": ATTITUDE.yaw,
            "terrainAhead":0,
            "timestamp":timestamp,
            "roll":ATTITUDE.roll,
            "pitch":ATTITUDE.pitch,
            "yaw":ATTITUDE.yaw,
            "rollspeed":ATTITUDE.rollspeed,
            "pitchspeed":ATTITUDE.pitchspeed,
            "yawspeed":ATTITUDE.yawspeed,
            "press_abs":SCALED_PRESSURE.press_abs,
            "press_diff":SCALED_PRESSURE.press_diff,
            "temperature":SCALED_PRESSURE.temperature,
            "load":SYS_STATUS.load,
            "voltage_battery":SYS_STATUS.voltage_battery,
            "current_battery":SYS_STATUS.current_battery,
            "relative_alt":GLOBAL_POSITION_INT.relative_alt,
            "base_mode":HEARTBEAT.base_mode,
            "custom_mode":HEARTBEAT.custom_mode
            }
        self.db.session.add(aux)
        self.db.session.commit()
        
    def get(self):
        
        if self.telemetria != {} :
            print(jsonify(self.telemetria))
            return jsonify(self.telemetria)
        else:
            return "{}"
       
       

    def setUpdate(self):
        while True:
            self.heartbeat()
            self.getData()
            time.sleep(1)

app=Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"]='mysql+pymysql://root:root@localhost/drone'

db = SQLAlchemy(app)

class Telemetria(db.Model):
    __tablename__ = 'telemetria'
    _id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nome = db.Column(db.String(255))
    airspeed = db.Column(db.Float)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    trueCourse = db.Column(db.Float)
    heading = db.Column(db.Float)
    headingBug = db.Column(db.Float)
    groundSpeed = db.Column(db.Float)
    airspeedBug = db.Column(db.Float)
    altitude = db.Column(db.Float)
    altitudeBug = db.Column(db.Float)
    verticalSpeed = db.Column(db.Float)
    verticalSpeedBug = db.Column(db.Float)
    bankAngle = db.Column(db.Float)
    pitchAngle = db.Column(db.Float)
    turnCoordinationAngle = db.Column(db.Float)
    terrainAhead = db.Column(db.Float)
    timestamp=db.Column(db.Float)
    
    satellites_visible=db.Column(db.Float)
    hdop=db.Column(db.Float)
    roll=db.Column(db.Float)
    pitch=db.Column(db.Float)
    yaw=db.Column(db.Float)
    rollspeed=db.Column(db.Float)
    pitchspeed=db.Column(db.Float)
    yawspeed=db.Column(db.Float)
    press_abs=db.Column(db.Float)
    press_diff=db.Column(db.Float)
    temperature=db.Column(db.Float)

    load=db.Column(db.Float)
    voltage_battery=db.Column(db.Float)
    current_battery=db.Column(db.Float)
    relative_alt=db.Column(db.Float)
    base_mode=db.Column(db.Integer)
    custom_mode=db.Column(db.Integer)
    
    def __init__(self,airspeed,latitude,longitude,satellites_visible,hdop,trueCourse,heading,headingBug,groundSpeed, airspeedBug,altitude,altitudeBug,verticalSpeed, verticalSpeedBug,bankAngle, pitchAngle,turnCoordinationAngle,terrainAhead, timestamp,roll, pitch, yaw,rollspeed, pitchspeed,yawspeed,press_abs,press_diff,temperature,load,voltage_battery,current_battery,base_mode,custom_mode,relative_alt,nome="default"):
        self.nome = nome
        self.airspeed=airspeed
        self.latitude= latitude
        self.longitude= longitude
        self.satellites_visible=satellites_visible
        self. hdop=hdop
        self.trueCourse= trueCourse
        self.heading= heading
        self.headingBug= headingBug
        self.groundSpeed=groundSpeed
        self.airspeedBug= airspeedBug
        self.altitude= altitude
        self.altitudeBug= altitudeBug		
        self.verticalSpeed= verticalSpeed
        self.verticalSpeedBug= verticalSpeedBug		
        self.bankAngle=bankAngle
        self.pitchAngle= pitchAngle
        self.turnCoordinationAngle= turnCoordinationAngle
        self.terrainAhead= terrainAhead
        self. timestamp=timestamp
        self. roll=roll
        self.pitch=pitch
        self. yaw=yaw
        self.rollspeed=rollspeed
        self. pitchspeed=pitchspeed
        self.yawspeed=yawspeed
        self.press_abs=press_abs
        self.press_diff=press_diff
        self.temperature=temperature
        self.load=load
        self.voltage_battery=voltage_battery
        self.current_battery=current_battery
        self.relative_alt=relative_alt
        self.base_mode=base_mode
        self.custom_mode=custom_mode

db.create_all()

CORS(app)
d=drone('udp:localhost:14551',db)  
@app.route("/")
def get():
    print("d.get()")
    return d.get()

if(__name__ == "__main__"):
    app.run(debug=True)
    # app.run()



    