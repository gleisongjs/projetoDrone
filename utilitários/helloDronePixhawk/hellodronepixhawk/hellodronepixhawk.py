#!/usr/bin/python
#-*-coding: utf-8 -*-
#Criado por GleisonJSilva
#Criado em : 21/01/2018, 01:09:34

import pymavlink.mavutil as mavutil
import sys
import time

mav = mavutil.mavlink_connection('/dev/ttyACM0')

mav.wait_heartbeat()
mav.mav.command_long_send(mav.target_system, mav.target_component,
                          mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM, 0, 1,
                          0, 0, 0, 0, 0, 0)#param1 = 1 é Armar 0 é Desligar
time.sleep(2)

# @type mav 
mav.mav.command_long_send(mav.target_system, mav.target_component, mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,0,0,0,0,0,0,0,2)

#MAV_CMD_NAV_TAKEOFF - Comando de TakeOff Decolar 
#param1 	Grau % 	Ângulo de inclinação / escalada (apenas plano).
#param2 		Vazio
#param3 		Vazio
#param4 		Ângulo da ala (ignorado se a bússola não estiver presente).
#param5 	Lat 	Latitude
#param6 	Lon 	Longitude
#param7 	Alt 	Altitude

#mav.mav.command_long_send(mav.target_system, mav.target_component,
#                          mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM, 0, 0,
#                          0, 0, 0, 0, 0, 0)
time.sleep(10)
