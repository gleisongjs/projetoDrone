#!/usr/bin/python
#-*-coding: utf-8 -*-
#Criado por GleisonJSilva
#Criado em : 13/01/2018, 13:43:47

import pymavlink.mavutil as mavutil
import time


#modo 0 - STABILIZE - O modo stabilize não possui controle automático de altitude nem de posicionamento(GPS), é um dos modos mais difíceis, pois você tem que regular o acelerador constantemente, 
#modo 1 = acro - O Acro Mode, como o nome sugere é destinado a acrobacias e só deve ser utilizado por pilotos experientes
#modo 2 = ALT_HOLD - No modo Alt Hold a aceleração é controlada automaticamente visando manter o drone na mesma altitude, é um modo fácil de pilotagem, tendo em vista que você vai se preocupar mais com o controle da direita,
#modo 3 = AUTO - O modo auto é utilizado quando você programou uma missão previamente (waypoints) e chegou no ponto em que deseja executá-la
#modo 4 = GUIDED - O modo guided é mais ou menos como se fossem missões dinâmicas, ao invés de utilizar um controle, você comanda o voo através de waypoints direto da groundstation (computador / tablet).
#modo 5 = LOITER - O modo Loiter é o modo GPS, o multirotor vai tentar manter sua posição atual, um bom sinal de GPS é essencial para um voo tranquilo
#modo 6 = RTL - O modo RTL é operado em conjunto com GPS também(e depende de um bom sinal) e tem a função de trazer o drone para o home point gravado, ou seja, a posição de decolagem. 
#modo 7 = CIRCLE - Nesse modo o drone vai ficar voando em círculos, o raio deve ser definido no parâmetro CIRCLE_RADIUS.
#modo 8 = modo não encontrado
#modo 9 = LAND - O modo Land é raramente usado, serve para pousar, mas não compensa gastar uma posição de chave do rádio com ele.
#modo 10 - não encontrado
#modo 11 - DRIFT - Nesse modo o Roll é controlado automaticamente e os outros controles ficam disponíveis para o piloto, o controle com uma mão é possível em boa parte do tempo, o YAW não é utilizado e você só vai utilizar o controle esquerdo quando quiser mudar de altitude, é similar ao controle de um manche.
#Alguns iniciantes testam o modo e nunca mais voltam para outro, também é muito utilizado para FPV e filmagens mais fluídas.
#modo 12 - não encontrado
#modo 13 - SPORT - No modo Sport o estabilizador é parcialmente utilizado e o controle de altitude está operante, é um modo bom para FPV porque você pode manter o drone em um determinado ângulo.
#modo 14 - não encontrado




#conexao com o simulador
mav = mavutil.mavlink_connection('udp:localhost:14551')

#Utilizando telemetria
#mav = mavutil.mavlink_connection('/dev/ttyUSB0', 57600)


#Drone Tiago
#mav = mavutil.mavlink_connection('tcp:192.168.0.1:9876')

mav.wait_heartbeat()

#Setando modo GUIDED
mav.set_mode(4)
#comando simplificado para armar
mav.arducopter_arm()

time.sleep(5)


mav.mav.command_long_send(mav.target_system, mav.target_component, 
                 mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,
                 0, # confirmation
                 0, # param1
                 0, # param2
                 0, # param3
                 0, # param4
                 0, # param5
                 0, # param6
                 10)


time.sleep(20)


mav.mav.command_long_send(mav.target_system, mav.target_component, 
                 mavutil.mavlink.MAV_CMD_NAV_LOITER_TO_ALT,
                 0, # confirmation
                 0, # param1
                 0, # param2
                 0, # param3
                 0, # param4
                 0, # param5
                 0, # param6
                 20)
 
time.sleep(10)

#POUSAR
mav.set_mode(9)

mav.mav.command_long_send(mav.target_system, mav.target_component,
                           mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM, 0, 0,
                           0, 0, 0, 0, 0, 0)