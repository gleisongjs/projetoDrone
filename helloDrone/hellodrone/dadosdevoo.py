#!/usr/bin/python
#-*-coding: utf-8 -*-
#Criado por GleisonJSilva
#Criado em : 07/08/2019

import pymavlink.mavutil as mavutil
import paho.mqtt.publish as publish
import psycopg2
import sys
import time


#modo 0 - STABILIZE - O modo stabilize não possui controle automático de altitude nem de posicionamento(GPS), é um dos modos mais difíceis, pois você tem que regular o acelerador constantemente, 
#modo 1 = acro - O Acro Mode, como o nome sugere é destinado a acrobacias e só deve ser utilizado por pilotos experientes
#modo 2 = ALT_HOLD - No modo Alt Hold a aceleração é controlada automaticamente visando manter o drone na mesma altitude, é um modo fácil de pilotagem, tendo em vista que você vai se preocupar mais com o controle da direita,
#modo 3 = AUTO - O modo auto é utilizado quando você programou uma missão previamente (waypoints) e chegou no ponto em que deseja executá-la
#modo 4 = GUIDED - O modo guided é mais ou menos como se fossem missões dinâmicas, ao invés de utilizar um controle, você comanda o voo através de waypoints direto da groundstation (computador / tablet).
#modo 5 = LOITER - O modo Loiter é o modo GPS, o multirotor vai tentar manter sua posição atual, um bom sinal de GPS é essencial para um voo tranquilo
#modo 6 = RTL - O modo RTL é operado em conjunto com GPS também(e depende de um bom sinal) e tem a função de trazer o drone para o home point gravado, ou seja, a posição de decolagem. 
#modo 7 = CIRCLE - Nesse modo o drone vai ficar voando em círculos, o raio deve ser definido no parâmetro CIRCLE_RADIUS.
#modo 8 = modo não encontrado
#modo 9 = LAND - O modo Land é raramente usado, serve para pousar, mas não compensa gastar uma posição de chave do rádio com ele.
#modo 10 - não encontrado
#modo 11 - DRIFT - Nesse modo o Roll é controlado automaticamente e os outros controles ficam disponíveis para o piloto, o controle com uma mão é possível em boa parte do tempo, o YAW não é utilizado e você só vai utilizar o controle esquerdo quando quiser mudar de altitude, é similar ao controle de um manche.
#Alguns iniciantes testam o modo e nunca mais voltam para outro, também é muito utilizado para FPV e filmagens mais fluídas.
#modo 12 - não encontrado
#modo 13 - SPORT - No modo Sport o estabilizador é parcialmente utilizado e o controle de altitude está operante, é um modo bom para FPV porque você pode manter o drone em um determinado ângulo.
#modo 14 - não encontrado



con = psycopg2.connect(host='localhost', database='dadosdevoo',
#con = psycopg2.connect(host='localhost', database='drone',
user = 'gleisongjs', password = 'jsilva996')

#conexao com simulador
#mav = mavutil.mavlink_connection('udp:localhost:14551')

#conexao com Pixhawk
mav = mavutil.mavlink_connection('/dev/ttyACM0')

mav.wait_heartbeat()


def iniciaPx4():
    mav.mav.request_data_stream_send(mav.target_system, mav.target_component,
                                    mavutil.mavlink.MAV_DATA_STREAM_ALL,4,  1)

def conexao():
        con = psycopg2.connect(host='localhost', database='dadosdevoo',
        user = 'gleisongjs', password = 'jsilva996')

def salvarDados():
    
    
    params = mav.recv_match(type='VFR_HUD', blocking=True)
    HEARTBEAT = mav.recv_match(type='HEARTBEAT', blocking=True)
    SYS_STATUS = mav.recv_match(type='SYS_STATUS', blocking=True)
    GPS_RAW_INT = mav.recv_match(type='GPS_RAW_INT', blocking=True)
    GLOBAL_POSITION_INT = mav.recv_match(type='GLOBAL_POSITION_INT', blocking=True)
    SCALED_PRESSURE = mav.recv_match(type='SCALED_PRESSURE', blocking=True)
    ATTITUDE = mav.recv_match(type='ATTITUDE', blocking=True)
    TEMPO = mav.recv_match(type='SYSTEM_TIME', blocking=True)
    COORDENADAS = mav.recv_match(type='GLOBAL_POSITION_INT', blocking=True)

    #MODELOpublish.single("dadosdevoo/telemetria/airspeed", airspeed, hostname="172.24.1.1")
    airspeed = (params.airspeed)*3.6
    publish.single("dadosdevoo/telemetria/airspeed", airspeed, hostname="127.0.0.1")
    latitude = COORDENADAS.lat
    publish.single("dadosdevoo/telemetria/latitude", latitude, hostname="127.0.0.1")
    longitude = COORDENADAS.lon
    publish.single("dadosdevoo/telemetria/longitude", longitude, hostname="127.0.0.1")
    heading = params.heading
    publish.single("dadosdevoo/telemetria/heading", heading, hostname="127.0.0.1")
    groundspeed = (GPS_RAW_INT.vel) 
    publish.single("dadosdevoo/telemetria/groundspeed", groundspeed, hostname="127.0.0.1")       	
    altitude = (GPS_RAW_INT.alt)/1000
    publish.single("dadosdevoo/telemetria/altitude", altitude, hostname="127.0.0.1")
    verticalspeed = params.climb
    publish.single("dadosdevoo/telemetria/verticalspeed", verticalspeed, hostname="127.0.0.1")
    bankangle = ATTITUDE.roll*-100
    publish.single("dadosdevoo/telemetria/bankangle", bankangle, hostname="127.0.0.1")
    pitchangle = ATTITUDE.pitch*100
    publish.single("dadosdevoo/telemetria/pitchangle", pitchangle, hostname="127.0.0.1")
    turncoordinationangle = ATTITUDE.yaw
    publish.single("dadosdevoo/telemetria/turncoordinationangle", turncoordinationangle, hostname="127.0.0.1")        
    roll = ATTITUDE.roll
    publish.single("dadosdevoo/telemetria/roll", roll, hostname="127.0.0.1")
    pitch = ATTITUDE.pitch
    publish.single("dadosdevoo/telemetria/pitch", pitch, hostname="127.0.0.1")
    yaw = ATTITUDE.yaw
    publish.single("dadosdevoo/telemetria/yaw", yaw, hostname="127.0.0.1")
    rollspeed = ATTITUDE.rollspeed
    publish.single("dadosdevoo/telemetria/rollspeed", rollspeed, hostname="127.0.0.1")
    pitchspeed = ATTITUDE.pitchspeed
    publish.single("dadosdevoo/telemetria/pitchspeed", pitchspeed, hostname="127.0.0.1")
    yawspeed = ATTITUDE.yawspeed
    publish.single("dadosdevoo/telemetria/yawspeed", yawspeed, hostname="127.0.0.1")
    temperature = SCALED_PRESSURE.temperature
    publish.single("dadosdevoo/telemetria/temperature", temperature, hostname="127.0.0.1")
    tempo = (TEMPO.time_unix_usec)/1000000
    #publish.single("dadosdevoo/telemetria/tempo", tempo, hostname="127.0.0.1")

    #banco de dados
    cur = con.cursor()
    sql = 'insert into telemetria (airspeed, latitude, longitude, altitude, heading, groundspeed, verticalspeed, bankangle, pitchangle, turncoordinationangle, roll, pitch, yaw, rollspeed, pitchspeed, yawspeed, temperature, tempo) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) '
    sqlDados = (airspeed, latitude, longitude, altitude, heading, groundspeed, verticalspeed, bankangle, pitchangle, turncoordinationangle, roll, pitch, yaw, rollspeed, pitchspeed, yawspeed, temperature, tempo)
    cur.execute(sql, sqlDados)
    con.commit()
    print 'Dispositivo adicionado com sucesso!\n'

#iniciando a transmissão de dados do pixhawk
iniciaPx4()

while True:
    
    # if (level == 0):
    #     publish.single("dadosdevoo/telemetria/salvardados", "Não salvando dados", hostname="127.0.0.1")
    #     print 'Não salvando dados'
    #     param = (str(mav.recv_match(type='GPS_RAW_INT', blocking=True)))
    #     print param
    #     time.sleep(1)
    # else:
        publish.single("dadosdevoo/telemetria/salvardados", "salvando dados", hostname="127.0.0.1")
        print 'salvando dados'
        salvarDados()
        # Publica
        param = (str(mav.recv_match(type='VFR_HUD', blocking=True)))
        #publish.single("dadosdevoo/telemetria", param, hostname="192.168.0.71")
    
        print param
        time.sleep(1)
    

