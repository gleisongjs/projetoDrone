#https://www.ardusub.com/developers/pymavlink.html
# Import mavutil
from flask import Flask,jsonify
from pymavlink import mavutil
import time
from flask_cors import CORS
import psycopg2

def conexao():
        con = psycopg2.connect(host='localhost', database='drone',
        user = 'postgres', password = '')

def salvarDados():
    
    
    params = self.master.recv_match(type='VFR_HUD', blocking=True)
    HEARTBEAT = self.master.recv_match(type='HEARTBEAT', blocking=True)
    SYS_STATUS = self.master.recv_match(type='SYS_STATUS', blocking=True)
    GPS_RAW_INT = self.master.recv_match(type='GPS_RAW_INT', blocking=True)
    GLOBAL_POSITION_INT = self.master.recv_match(type='GLOBAL_POSITION_INT', blocking=True)
    SCALED_PRESSURE = self.master.recv_match(type='SCALED_PRESSURE', blocking=True)
    ATTITUDE = self.master.recv_match(type='ATTITUDE', blocking=True)

    airspeed = (params.airspeed)*3.6
    latitude = GPS_RAW_INT.lat
    longitude = GPS_RAW_INT.lon
    heading = params.heading
    groundSpeed = params.groundspeed         	
    altitude = params.alt
    verticalSpeed = params.climb
    bankAngle = ATTITUDE.roll*-100
    pitchAngle = ATTITUDE.pitch*100
    turnCoordinationAngle = ATTITUDE.yaw        
    roll = ATTITUDE.roll
    pitch = ATTITUDE.pitch
    yaw = ATTITUDE.yaw
    rollspeed = ATTITUDE.rollspeed
    pitchspeed = ATTITUDE.pitchspeed
    yawspeed = ATTITUDE.yawspeed
    temperature = SCALED_PRESSURE.temperature

    cur = con.cursor()
    sql = 'insert into telemetria (airspeed, latitude, longitude, heading, groundSpeed, altitude, verticalSpeed, bankAngle, pitchAngle, turnCoordinationAngle, roll, pitch, yaw, rollspeed, pitchspeed, yawspeed, temperature) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) '
    sqlDados = (airspeed, latitude, longitude, heading, groundSpeed, altitude, verticalSpeed, bankAngle, pitchAngle, turnCoordinationAngle, roll, pitch, yaw, rollspeed, pitchspeed, yawspeed, temperature)
    cur.execute(sql, sqlDados)
    con.commit()
    print 'Dispositivo adicionado com sucesso!\n'

class drone(object):
    

    def __init__(self, url):
        self.master = mavutil.mavlink_connection(url)
        #self.master.arducopter_arm()
    def teste(self):
        print ("url")
        while True:
            try:
                print(self.master.recv_match().to_dict())

            except:
                pass
    def armar(self):
        self.master.mav.command_long_send(
            self.master.target_system,
            self.master.target_component,
            mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
            0,1, 0, 0, 0, 0, 0, 0)
    def desarmar(self):
        self.master.mav.command_long_send(
            self.master.target_system,
            self.master.target_component,
            mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
            0,0, 0, 0, 0, 0, 0, 0)
    def takeoff(self,altitude):
        self.master.mav.command_long_send(self.master.target_system, self.master.target_component, 
                mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,
                0, # confirmation
                0, # param1
                0, # param2
                0, # param3
                0, # param4
                0, # param5
                0, # param6
                altitude)
        
    def modo_estabilizado(self):
        return self.setModoDeVoo("STABILIZE")
    def modo_guiado(self):
        return self.setModoDeVoo('GUIDED')
    def modo_loiter(self):
        return self.setModoDeVoo('LOITER')
    def modo_rtl(self):
        return self.setModoDeVoo('RTL')
    def modoCirculo(self):
        return self.master.set_mode(7)
    def setModoDeVoo(self,mode):
        if mode not in self.master.mode_mapping():
            print('Unknown mode : {}'.format(mode))
            print('Try:', list(self.master.mode_mapping().keys()))
            exit(1)
        mode_id = self.master.mode_mapping()[mode]
        # Set new mode
        # master.mav.command_long_send(
        #    master.target_system, master.target_component,
        #    mavutil.mavlink.MAV_CMD_DO_SET_MODE, 0,
        #    0, mode_id, 0, 0, 0, 0, 0) or:
        # master.set_mode(mode_id) or:
        self.master.mav.set_mode_send(
            self.master.target_system,
            mavutil.mavlink.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED,
            mode_id)
        # Check ACK
        ack = False
        while not ack:
            # Wait for ACK command
            ack_msg = self.master.recv_match(type='COMMAND_ACK', blocking=True)
            ack_msg = ack_msg.to_dict()
            # Check if command in the same in `set_mode`
            if ack_msg['command'] != mavutil.mavlink.MAVLINK_MSG_ID_SET_MODE:
                continue
            # Print the ACK result !
            resultado=mavutil.mavlink.enums['MAV_RESULT'][ack_msg['result']].description
            print(resultado)
            if(resultado=="Command ACCEPTED and EXECUTED"):
                return True
            else:
                return False
    def set_rc_channel_pwm(id, pwm=1500):
        """ Set RC channel pwm value
        Args:
            id (TYPE): Channel ID
            pwm (int, optional): Channel pwm value 1100-1900
        """
        if id < 1:
            print("Channel does not exist.")
            return

        # We only have 8 channels
        #http://mavlink.org/messages/common#RC_CHANNELS_OVERRIDE
        if id < 9:
            rc_channel_values = [65535 for _ in range(8)]
            rc_channel_values[id - 1] = pwm
            self.master.mav.rc_channels_override_send(
            self.master.target_system,                # target_system
            self.master.target_component,             # target_component
                *rc_channel_values)                  # RC channel list, in microseconds.
    def circulo(self,altitude=0,qnt_voltas=1,raio=1,latitude=0,longitude=0):
        self.master.mav.command_long_send(self.master.target_system, self.master.target_component, 
                mavutil.mavlink.MAV_CMD_NAV_LOITER_TURNS,
                0,
                 qnt_voltas, # confirmation
                0, # param1
                raio, # param2
                 0, # param3
                 latitude, # param4
                 longitude, # param5
                 altitude)
    def get(self):
        params = self.master.recv_match(type='VFR_HUD', blocking=True)
        HEARTBEAT = self.master.recv_match(type='HEARTBEAT', blocking=True)
        SYS_STATUS = self.master.recv_match(type='SYS_STATUS', blocking=True)
        #GPS_RAW_INT = self.master.recv_match(type='GPS_RAW_INT', blocking=True)
        #GLOBAL_POSITION_INT = self.master.recv_match(type='GLOBAL_POSITION_INT', blocking=True)
        #SCALED_PRESSURE = self.master.recv_match(type='SCALED_PRESSURE', blocking=True)
        ATTITUDE = self.master.recv_match(type='ATTITUDE', blocking=True)



        msg={
        "airspeed":(params.airspeed)*3.6,
        "airspeedBug": (params.airspeed)*3.6,
        #"latitude":GPS_RAW_INT.lat,
        #"longitude": GPS_RAW_INT.lon,
        #"satellites_visible":GPS_RAW_INT.satellites_visible,
        #"hdop":GPS_RAW_INT.eph,
        "trueCourse": 0,
        "heading": params.heading,
        "headingBug": params.heading,
        "groundSpeed": params.groundspeed,         	
        "altitude": params.alt,
        "altitudeBug": params.alt,		
        "verticalSpeed": params.climb,
        "verticalSpeedBug": params.climb,		
        "bankAngle":ATTITUDE.roll*-100,
        "pitchAngle": ATTITUDE.pitch*100,
        "turnCoordinationAngle": ATTITUDE.yaw,
        "terrainAhead":0,
        "roll":ATTITUDE.roll,
        "pitch":ATTITUDE.pitch,
        "yaw":ATTITUDE.yaw,
        "rollspeed":ATTITUDE.rollspeed,
        "pitchspeed":ATTITUDE.pitchspeed,
        "yawspeed":ATTITUDE.yawspeed,
        #"press_abs":SCALED_PRESSURE.press_abs,
        #"press_diff":SCALED_PRESSURE.press_diff,
        #"temperature":SCALED_PRESSURE.temperature,
        #"load":SYS_STATUS.load,
        #"voltage_battery":SYS_STATUS.voltage_battery,
        #"current_battery":SYS_STATUS.current_battery,
        #"relative_alt":GLOBAL_POSITION_INT.relative_alt,
        #"base_mode":HEARTBEAT.base_mode,
        #"custom_mode":HEARTBEAT.custom_mode
    }
        return msg

       

    def heartbeat(self):
        self.master.wait_heartbeat()

app=Flask(__name__)
CORS(app)
@app.route("/init")
def init():
    pass
    #d=drone("tcp:127.0.0.1:5760")
@app.route("/")
def get():
    d.heartbeat()
    print(d.get())
    imprime = d.get()
    #print imprime
    return jsonify(imprime)

#d=drone('udp:localhost:14551')
#d=drone('/dev/ttyUSB0', 57600)
d=drone('/dev/ttyACM0')


if(__name__ == "__main__"):
    
    d.heartbeat()
    app.run()
    d.get()
    #conexao()
    #salvarDados()
    
    
    #d.modo_guiado()d.get()
    #d.armar()
    #d.takeoff(2)
    #time.sleep(2)
    #d.modoCirculo()
    
    